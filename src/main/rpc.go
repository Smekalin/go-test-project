package main

import (
	"net/rpc"
	"net"
	"fmt"
)


type Server struct {}

func (this *Server) Hello(msg string, reply *string) error {
	*reply = msg + " World"
	return nil
}

func server() {
	rpc.Register(new(Server))
	ln, err := net.Listen("tcp", ":9999")
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		c, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}
		go rpc.ServeConn(c)
	}
}


func client() {
	c, err := rpc.Dial("tcp", "127.0.0.1:9999")
	if err != nil {
		fmt.Println(err)
		return
	}
	var result string
	err = c.Call("Server.Hello", "Hello", &result)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Server.Hello = ", result)
	}
}

func main () {
	go server()
	go client()

	var a string
	fmt.Scanln(&a)
}
