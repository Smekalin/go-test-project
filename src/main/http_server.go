package main

import (
	"net/http"
	"fmt"
	"io/ioutil"
	"io"
)

func hello(res http.ResponseWriter, req *http.Request) {

	res.Header().Set(
		"Content-Type",
		"text/html",
	)
	// read .html
	filename := "./hello.html"
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	// add html page in response
	io.WriteString(res, string(bs))
}

func prepareServer() {
	http.Handle("/static/",
					http.StripPrefix(
						"/static/",
						http.FileServer(http.Dir("static")),
					),
				)
	http.HandleFunc("/hello", hello)
}

func main() {
	prepareServer()
	http.ListenAndServe("127.0.0.1:8080", nil)
}
