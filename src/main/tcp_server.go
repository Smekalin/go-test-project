package main

import (
	"net"
	"fmt"
	"encoding/gob"
	"time"
)

func server() {
	// listen
	ln, err := net.Listen("tcp", ":9999")

	if err != nil {
		fmt.Println(err)
		return
	}

	// accept
	for {
		client, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}
		go handleServerConnection(client)
	}

}

func handleServerConnection(client net.Conn) {
	// receive conn
	var msg string
	err := gob.NewDecoder(client).Decode(&msg)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Received: ", msg)
	}

	client.Close()
}

func client(i int) {
	for {
		// connect to the server
		conn, err := net.Dial("tcp", "127.0.0.1:9999")
		if err != nil {
			fmt.Println(err)
			return
		}

		// send the message
		msg := "Hello from " + fmt.Sprintf("%d", i)
		err = gob.NewEncoder(conn).Encode(msg)

		time.Sleep(time.Second * 2)

		if err != nil {
			fmt.Println(err)
		}

		conn.Close()
	}
}


func main() {
	go server()
	for i := 0; i < 10; i++ {
		go client(i)
	}

	var input string
	fmt.Scanln(&input)
}